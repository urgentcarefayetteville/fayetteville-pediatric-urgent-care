**Fayetteville pediatric urgent care**

Pediatric Urgent Care Fayetteville is here to support us with our extended hours. 
Although some of the challenges include driving to the emergency department, 
most pediatric illnesses and injuries will be treated in either of our Pediatric Emergency Care Fayetteville. 
As well as common on-site laboratory testing, we have on-site x-rays.
The results are readily accessible, so you don't have to go home wondering if your child's going to be all right.
Please Visit Our Website [Fayetteville pediatric urgent care](https://urgentcarefayetteville.com/pediatric-urgent-care.php) for more information. 
---

## Our pediatric urgent care in Fayetteville services

Hospital care in children offers support for several injuries. 
We will stitch lacerations and, if necessary, have surgical glue instead of stitches. We will splint broken bones and take care of the sprains and strains. 
As early as newborns and infants, Fayetteville Pediatric Emergency Services evaluates and controls most pediatric conditions.
We also send in-house meds to make your life easier. 
If you need to see a doctor NOW for flu, colds or other illnesses, Fayetteville Pediatric Urgent Care is simple and available 7 days a week without an appointment. 
A year, 365 days a year. Every part of our team has the same patient focus and is committed to providing the best possible care from board-certified doctors.
